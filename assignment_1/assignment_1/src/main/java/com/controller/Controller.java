package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.services.Services;
import com.weatherEntity.WeatherDetails;

@RestController
@RequestMapping("/weatherForecast")
public class Controller {

	@Autowired
	private Services weatherService;

	@GetMapping("/{city}")
	public ResponseEntity<Object> getWeatherByCity(@PathVariable String city) {
		return new ResponseEntity<Object>(weatherService.getWeatherByCity(city), HttpStatus.ACCEPTED);
	}

	@PostMapping("/save")
	public ResponseEntity<List<WeatherDetails>> saveMultipleCitiesWeather(@RequestBody List<WeatherDetails> weather) {
		return new ResponseEntity<List<WeatherDetails>>(weatherService.saveMultiplecitiesWeather(weather),
				HttpStatus.CREATED);
	}
	
	@PostMapping("/saveWeather")
	public ResponseEntity<WeatherDetails> saveCityWeather(@RequestBody WeatherDetails weather) {
		return new ResponseEntity<WeatherDetails>(weatherService.saveWeather(weather), HttpStatus.CREATED);
	}

	@GetMapping("/getAllCities")
	public ResponseEntity<List<WeatherDetails>> getAllCitiesWeatherInfo(@RequestParam String field) {
		return new ResponseEntity<List<WeatherDetails>>(weatherService.getAllCitiesWeather(field), HttpStatus.OK);
	}

	@GetMapping("/getCityByName/{city}")
	public ResponseEntity<WeatherDetails> getWeatherByCityName(@PathVariable String city) {
		return new ResponseEntity<WeatherDetails>(weatherService.getWeatherByCityName(city), HttpStatus.ACCEPTED);
	}

	@GetMapping("/average")
	public ResponseEntity<String> getAvgWeatherForCity() {

		double avgTemperature = weatherService.gettingAvgTemparature();
		double avgPressure = weatherService.gettingAvgPressure();
		String avg = "Average Temparature :" + avgTemperature + "Average Pressure:" + avgPressure;
		return new ResponseEntity<String>(avg, HttpStatus.OK);
	}
}
