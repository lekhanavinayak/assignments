package com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.exceptions.CityNotFoundEx;
import com.repo.WeatherRepository;
import com.weatherEntity.WeatherDetails;

@Service
public class Services {

	private static final String api_key = "83eb87845acd24c4022ce5306b24e88a";
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private WeatherRepository weatherRepo;

	public Object getWeatherByCity(String city) {
		
		return restTemplate.exchange(
				"https://api.openweathermap.org/data/2.5/forecast?q={city}&units=metric&cnt=16&appid=" + api_key,
				HttpMethod.GET, null, Object.class, city);
	}

	public WeatherDetails saveWeather(WeatherDetails weather) {
		return weatherRepo.save(weather);
	}

	public List<WeatherDetails> saveMultiplecitiesWeather(List<WeatherDetails> weather) {
		return weatherRepo.saveAll(weather);
	}

	public List<WeatherDetails> getAllCitiesWeather(String field) {
		List<WeatherDetails> weatherListOfAllCities = weatherRepo.findAll(Sort.by(Direction.ASC, field));
		if (weatherListOfAllCities.size() != 0) {
			return weatherListOfAllCities;
		} else {
			throw new CityNotFoundEx("Weather details not found");
		}
	}

	public WeatherDetails getWeatherByCityName(String city) {
		WeatherDetails findByCityName = weatherRepo.findByCityName(city);
		if (findByCityName != null && city != "" || city != null) {
			return findByCityName;
		} else {
			throw new CityNotFoundEx("Weather details not found with the city name : " + city);
		}
	}

	public double gettingAvgPressure() {
		return weatherRepo.getAvgPressure();
	}

	public double gettingAvgTemparature() {
		return weatherRepo.getAvgTemparature();
	}
}
