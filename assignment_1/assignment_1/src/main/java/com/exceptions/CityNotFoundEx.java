package com.exceptions;

public class CityNotFoundEx  extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public CityNotFoundEx()
	{
		super();
	}
	public CityNotFoundEx(String message)
	{
		super(message);
	}
}
