package com.exceptions;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
@RestControllerAdvice
public class CustomEx {
	

		@ExceptionHandler(CityNotFoundEx.class)
		public ResponseEntity<Object> handleCityNotFound(CityNotFoundEx infe)
		{
			HttpStatus notFound = HttpStatus.NOT_FOUND;
			ApplicationEx apiException = new ApplicationEx(infe.getMessage(), notFound, ZonedDateTime.now(ZoneId.of("Asia/Kolkata")));
			return new ResponseEntity<Object>(apiException, notFound);
		}
	}


