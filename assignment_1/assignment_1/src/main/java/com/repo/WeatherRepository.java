package com.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.weatherEntity.Details;
import com.weatherEntity.WeatherDetails;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherDetails, Long> {

	WeatherDetails findByCityName(String city);
	
	@Query("select AVG(temp) from Details m")
	double getAvgTemparature();

	@Query(value="SELECT dl.dt_txt as data,m.Details_ID,m.FEELS_LIKE," + 
			"m.PRESSURE,m.TEMP,m.MAX_TEMP,m.MIN_TEMP, FROM Details m, " +
			"DETAILS dl,FORECAST_DETAILS fd WHERE dl.Details_FK_ID=m.MAIN_ID AND lower(fd,name)=lower(?1)",nativeQuery=true)
          List<Details>fetchchangeofdaily(String city);
	
	@Query(value="SELECT AVG(m.PRESSURE) as pressure from Details m WHERE lower(fd.name)=lower(?1)",nativeQuery=true)
	long getAvgPressure();
	
}

