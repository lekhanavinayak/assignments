package com.weatherEntity;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {

	private String name;
	@Embedded
	private Points coord;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Points getCoord() {
		return coord;
	}

	public void setCoord(Points coord) {
		this.coord = coord;
	}

}
