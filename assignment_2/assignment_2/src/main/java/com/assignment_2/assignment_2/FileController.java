package com.assignment_2.assignment_2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileController {

	@Autowired
	private FileService fileService;

	@GetMapping("/save")
	public ResponseEntity<ResponseMessage> saveImage() {
		try {
			fileService.saveImages();
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Stored Images in DB"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("failed"));
		}
	}

	@GetMapping("/search")
	public ResponseEntity<ResponseMessage> searchImage(@RequestParam(required=false) String type, 
			@RequestParam(required=false) String fileName,
			@RequestParam(required=false) String date,
			@RequestParam(required=false) String sort) {
		ResponseMessage message = new ResponseMessage();
		try {
			List<Images> imageList = fileService.searchImage(type, fileName, date, sort);
			message.setImages(imageList);
			message.setMessage("Retrieved Successfully");
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("failed"));
		}
	}

}
