package com.assignment_2.assignment_2;

import java.util.List;

public class ResponseMessage {
	private String message;

	private List<Images> images;

	public ResponseMessage() {
		this.message = message;
	}
	
	public ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Images> getImages() {
		return images;
	}

	public void setImages(List<Images> images) {
		this.images = images;
	}

}
